<?php

namespace MdToHtml\composer;

use Composer\Script\Event;
use voku\helper\HtmlMin;
use Symfony\Component\Finder\Finder;

class ScriptHandler
{

    /**
     * Handles input.
     */
    public static function transform(Event $event)
    {
        $arguments = $event->getArguments();
        $destination = array_pop($arguments);
        $origin = array_pop($arguments);
        $removeFirstLine = in_array('--remove-first-line', $arguments);

        if (is_dir($origin)) {
            if (!file_exists($destination)) {
                mkdir($destination, 0777, true);
            }
            $finder = new Finder();
            $finder->in($origin);
            $finder->files()->name('*.md');
            foreach ($finder as $file) {
                self::generateHtml(
                    $file->getContents(),
                    $destination . '/' . pathinfo($file->getFilename(), PATHINFO_FILENAME) . '.html',
                    $removeFirstLine
                );
            }
        } else {
            self::generateHtml(file_get_contents($origin), $destination, $removeFirstLine);
        }
    }

    /**
     * Generates a html from an md file.
     */
    protected static function generateHtml($originContents, $destination, $removeFirstLine)
    {
        $html = (new \Parsedown())->parse($originContents);
        if ($removeFirstLine) {
            $html = preg_replace('/^.+\n/', '', $html);
        }
        // Replace line breaks with spaces.
        $html = (new HtmlMin())->minify($html);
        file_put_contents($destination, $html);
    }
}
